#include "arvore_busca.h"

void inserir(no **raiz, no *inserido) {
	no **posInserido = buscar(raiz, inserido->chave);

	if (*posInserido == NULL) *posInserido = inserido;
	else
		fprintf(stderr, "Chave já está na arvore");
}

no **menor(no **raiz) {
	if ((*raiz)->esq == NULL) return raiz;
	else return &((*raiz)->esq);
}

no **maior(no **raiz) {
	if ((*raiz)->dir == NULL) return raiz;
	else return &((*raiz)->dir);
}

no **buscar(no **raiz, TYPE_CHAVE chave) {
	if (*raiz == NULL) return raiz;
	else if ((*raiz)->chave > chave) return &((*raiz)->esq);
	else if ((*raiz)->chave < chave) return &((*raiz)->dir);
	else return raiz;
}

void remover(no **raiz, TYPE_CHAVE chave) {
	no **posRemove = buscar(raiz, chave);

	if (*posRemove == NULL)
		fprintf(stderr, "Chave não encontrada");
	else {
		if ((*posRemove)->esq == NULL) {
			*posRemove = (*posRemove)->dir;
		}
		else {
			no **maiorSubstituto = maior(&((*posRemove)->esq));

			no *substituto = *maiorSubstituto;

			*maiorSubstituto = substituto->esq;

			substituto->dir = (*posRemove)->dir;
			substituto->esq = (*posRemove)->esq;

			free(*posRemove);

			*posRemove = substituto;
		}
	}
}

