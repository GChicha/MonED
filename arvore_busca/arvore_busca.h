#ifndef _BINARIA_BUSCA_
#define _BINARIA_BUSCA_

#include <stdlib.h>
#include <stdio.h>
#include "estrutura_no.h"

void inserir(no **raiz, no *inserido);
no **menor(no **raiz);
no **maior(no **raiz);
no **buscar(no **raiz, TYPE_CHAVE chave);
void remover(no **raiz, TYPE_CHAVE chave);

#endif
