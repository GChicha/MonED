#ifndef __ARVORE_AVL__
#define __ARVORE_AVL__

#include "../arvore_busca.h"

// public
void removeBalanceado(no **raiz, TYPE_CHAVE chave);
void inserirBalanceado(no **raiz, no *inserido);

// private
void balance(no **raiz);

void rotEsq(no **raiz);
void rotDir(no **raiz);

#endif
