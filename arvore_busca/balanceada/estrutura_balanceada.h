#ifndef _STRUCT_NO_
#define _STRUCT_NO_

typedef int TYPE_CHAVE;
typedef char TYPE_VALOR;

typedef struct NO {
	TYPE_CHAVE chave;
	TYPE_VALOR valor[20];

	int altura;

	struct NO *dir;
	struct NO *esq;
} no;

#endif
